﻿using UnityEngine;
using System.Collections;

public class AutomaticScroll : MonoBehaviour {

    void OnBecameInvisible()
    {
        //calculate current position
        Vector3 pos = gameObject.transform.position;
        float Y = pos.y - transform.localScale.y*2;
        //move to new position when invisible
        gameObject.transform.position = new Vector3(pos.x, Y, 0f);
        //print(name + ": " + transform.position);
    }
}
