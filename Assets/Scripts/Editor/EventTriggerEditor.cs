﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(EventTrigger))]
public class EventTriggerEditor : Editor {

	private bool isValidType( System.Type type )
	{
		if (type.Name == "Int32")
			return true;

		if (type.Name == "Single")
			return true;

		if (type.Name == "String")
			return true;

		if (type.Name == "Boolean")
			return true;

		return false;
	}

	public override void OnInspectorGUI()
	{
		EventTrigger eventTrigger = (EventTrigger)this.target;
		EventManager eventManager = EventManager.GetInstance();

		// retrieve all method names from the event manager. If there exists no methods, call RegisterMethods to create them.
		string[] methodNames = eventManager.GetAllMethodNames();
		if ( methodNames.Length == 0 )
		{
			eventManager.SendMessage( "RegisterMethods" );
			methodNames = eventManager.GetAllMethodNames();
		}

		if ( methodNames.Length == 0 )
			return;

		// search for the currently selected method
		int selectedMethodIndex = 0;
		for ( int i = 0; i < methodNames.Length; ++i )
		{
			if ( eventTrigger.parameterEntry.MethodName == methodNames[i] )
			{
				selectedMethodIndex = i;
				break;
			}
		}

		// if the method name is empty, the new parameter entry has to be created for sure.
		if ( eventTrigger.parameterEntry.MethodName == "" )
			selectedMethodIndex = -1;

		GUI.changed = false;
		// show a popup list with all possible method entries
		int newSelectedMethodIndex = EditorGUILayout.Popup( selectedMethodIndex < 0 ? 0 : selectedMethodIndex, methodNames );
		if( newSelectedMethodIndex != selectedMethodIndex )
			eventTrigger.parameterEntry = new ParameterEntry( eventManager.GetMethodEntry( methodNames[newSelectedMethodIndex] ) );


		// depending on the type of the paramater show different input fields where the paramete could be specified.
		switch( eventTrigger.parameterEntry.ParamType )
		{
			case ParameterType.Int:
				eventTrigger.parameterEntry.IntParameter = EditorGUILayout.IntField("Parameter: ", eventTrigger.parameterEntry.IntParameter );
				break;
			case ParameterType.Float:
				eventTrigger.parameterEntry.FloatParameter = EditorGUILayout.FloatField( "Parameter: ", eventTrigger.parameterEntry.FloatParameter );
				break;
			case ParameterType.String:
				eventTrigger.parameterEntry.StringParameter = EditorGUILayout.TextField( "Parameter: ", eventTrigger.parameterEntry.StringParameter );
				break;
			case ParameterType.Bool:
				eventTrigger.parameterEntry.BoolParameter = EditorGUILayout.Toggle( "Parameter: ", eventTrigger.parameterEntry.BoolParameter );
				break;
			default:
				break;

		}
		
		if (GUI.changed)
			EditorUtility.SetDirty(eventTrigger);
		
	}

}
