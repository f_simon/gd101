﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(Polygon2D))]
public class Polygon2DEditor : Editor 
{
    enum State { Hover, Drag, BoxSelect, DragSelected, RotateSelected, ScaleSelected, Extrude }

    const float clickRadius = 0.12f;

    bool editing;
    bool tabDown;
    State state;

    List<Vector3> keyPoints;
    
    Matrix4x4 worldToLocal;
    Quaternion inverseRotation;

    Vector3 mousePosition;
    Vector3 clickPosition;
    Vector3 screenMousePosition;
    MouseCursor mouseCursor = MouseCursor.Arrow;

    int dragIndex;
    List<int> selectedIndices = new List<int>();
    int nearestLine;
    Vector3 splitPosition;
    bool extrudeKeyDown;
    bool doExtrudeUpdate;

    #region Properties

    private Polygon2D polygon
    {
        get { return (Polygon2D)target; }
    }

    public KeyCode editKey
    {
        get { return (KeyCode)EditorPrefs.GetInt( "PolyMeshEditor_editKey", (int)KeyCode.Tab ); }
        set { EditorPrefs.SetInt( "PolyMeshEditor_editKey", (int)value ); }
    }

    public KeyCode selectAllKey
    {
        get { return (KeyCode)EditorPrefs.GetInt( "PolyMeshEditor_selectAllKey", (int)KeyCode.A ); }
        set { EditorPrefs.SetInt( "PolyMeshEditor_selectAllKey", (int)value ); }
    }

    public KeyCode splitKey
    {
        get { return (KeyCode)EditorPrefs.GetInt( "PolyMeshEditor_splitKey", (int)KeyCode.S ); }
        set { EditorPrefs.SetInt( "PolyMeshEditor_splitKey", (int)value ); }
    }

    public KeyCode extrudeKey
    {
        get { return (KeyCode)EditorPrefs.GetInt( "PolyMeshEditor_extrudeKey", (int)KeyCode.D ); }
        set { EditorPrefs.SetInt( "PolyMeshEditor_extrudeKey", (int)value ); }
    }

    bool control
    {
        get { return Application.platform == RuntimePlatform.OSXEditor ? Event.current.command : Event.current.control; }
    }

    static bool uvSettings
    {
        get { return EditorPrefs.GetBool( "PolyMeshEditor_uvSettings", false ); }
        set { EditorPrefs.SetBool( "PolyMeshEditor_uvSettings", value ); }
    }

    #endregion

    #region EditorGUI
    public override void OnInspectorGUI()
    {
        if ( target == null )
            return;

        if ( polygon.keyPoints.Count == 0 )
            CreateSquare( polygon, 0.5f );

        if ( editing )
        {
            if ( GUILayout.Button( "Stop Editing" ) )
            {
                editing = false;
            }
        }
        else 
        {
            if ( GUILayout.Button( "Start Editing" ) )
            {
                editing = true;
            }
        }

        if ( uvSettings = EditorGUILayout.Foldout( uvSettings, "UVs" ) )
        {
            var uvPosition = EditorGUILayout.Vector2Field( "Position", polygon.uvPosition );
            var uvScale = EditorGUILayout.FloatField( "Scale", polygon.uvScale );
            var uvRotation = EditorGUILayout.Slider( "Rotation", polygon.uvRotation, -180, 180 ) % 360;
            if ( uvRotation < -180 )
                uvRotation += 360;
            if ( GUI.changed )
            {
                polygon.uvPosition = uvPosition;
                polygon.uvScale = uvScale;
                polygon.uvRotation = uvRotation;
            }
            if ( GUILayout.Button( "Reset UVs" ) )
            {
                polygon.uvPosition = Vector3.zero;
                polygon.uvScale = 1;
                polygon.uvRotation = 0;
            }

            if ( GUI.changed )
                polygon.BuildMesh();
        }
    }

    #endregion

    #region SceneGUI

    void OnSceneGUI()
    {
        if ( target == null )
            return;

        if ( editing )
        {
            //Update lists
            if ( keyPoints == null )
            {
                keyPoints = new List<Vector3>( polygon.keyPoints );
            }

            //Load handle matrix
            Handles.matrix = polygon.transform.localToWorldMatrix;

            //Draw points and lines
            DrawAxis();
            Handles.color = Color.red;
            for ( int i = 0; i < keyPoints.Count; i++ )
            {
                Handles.color = nearestLine == i ? Color.blue : Color.black;
                DrawSegment( i );
                if ( selectedIndices.Contains( i ) )
                {
                    Handles.color = Color.blue;
                    DrawCircle( keyPoints[i], 0.08f );
                }
                else
                    Handles.color = Color.black;
                DrawKeyPoint( i );
            }

            //Quit on tool change
            if ( Event.current.type == EventType.KeyDown )
            {
                switch ( Event.current.keyCode )
                {
                    case KeyCode.Q:
                    case KeyCode.W:
                    case KeyCode.E:
                    case KeyCode.R:
                        return;
                }
            }

            //Quit if panning or no camera exists
            if ( Tools.current == Tool.View || (Event.current.isMouse && Event.current.button > 0) || Camera.current == null || Event.current.type == EventType.ScrollWheel )
                return;

            //Quit if laying out
            if ( Event.current.type == EventType.Layout )
            {
                HandleUtility.AddDefaultControl( GUIUtility.GetControlID( FocusType.Passive ) );
                return;
            }

            if ( Event.current.keyCode == extrudeKey )
            {
                if ( extrudeKeyDown )
                {
                    if ( Event.current.type == EventType.KeyUp )
                        extrudeKeyDown = false;
                }
                else if ( Event.current.type == EventType.KeyDown )
                    extrudeKeyDown = true;
            }

            //Cursor rectangle
            EditorGUIUtility.AddCursorRect( new Rect( 0, 0, Camera.current.pixelWidth, Camera.current.pixelHeight ), mouseCursor );
            mouseCursor = MouseCursor.Arrow;

            //Update matrices and snap
            worldToLocal = polygon.transform.worldToLocalMatrix;
            inverseRotation = Quaternion.Inverse( polygon.transform.rotation ) * Camera.current.transform.rotation;

            //Update mouse position
            screenMousePosition = new Vector3( Event.current.mousePosition.x, Camera.current.pixelHeight - Event.current.mousePosition.y );
            var plane = new Plane( -polygon.transform.forward, polygon.transform.position );
            var ray = Camera.current.ScreenPointToRay( screenMousePosition );
            float hit;
            if ( plane.Raycast( ray, out hit ) )
                mousePosition = worldToLocal.MultiplyPoint( ray.GetPoint( hit ) );
            else
                return;

            //Update nearest line and split position
            nearestLine = NearestLine( out splitPosition );

            //Update the state and repaint
            var newState = UpdateState();
            if ( state != newState )
                SetState( newState );
            HandleUtility.Repaint();
            Event.current.Use();
        }
    }

    #endregion

    #region States
    void SetState( State newState )
    {
        state = newState;
        switch ( state )
        {
            case State.Hover:
                break;
        }
    }

    //Update state
    State UpdateState()
    {
        switch ( state )
        {
            //Hovering
            case State.Hover:

                DrawNearestLineAndSplit();

                if ( Tools.current == Tool.Move && TryDragSelected() )
                    return State.DragSelected;
                if ( Tools.current == Tool.Rotate && TryRotateSelected() )
                    return State.RotateSelected;
                if ( Tools.current == Tool.Scale && TryScaleSelected() )
                    return State.ScaleSelected;
                if ( Tools.current == Tool.Move && TryExtrude() )
                    return State.Extrude;

                if ( TrySelectAll() )
                    return State.Hover;
                if ( TrySplitLine() )
                    return State.Hover;
                if ( TryDeleteSelected() )
                    return State.Hover;

                if ( TryHoverKeyPoint( out dragIndex ) && TryDragKeyPoint( dragIndex ) )
                    return State.Drag;
                if ( TryBoxSelect() )
                    return State.BoxSelect;

                break;

            //Dragging
            case State.Drag:
                mouseCursor = MouseCursor.MoveArrow;
                DrawCircle( keyPoints[dragIndex], clickRadius );
                MoveKeyPoint( dragIndex, mousePosition - clickPosition );
                if ( TryStopDrag() )
                    return State.Hover;
                break;

            //Box Selecting
            case State.BoxSelect:
                if ( TryBoxSelectEnd() )
                    return State.Hover;
                break;

            //Dragging selected
            case State.DragSelected:
                mouseCursor = MouseCursor.MoveArrow;
                MoveSelected( mousePosition - clickPosition );
                if ( TryStopDrag() )
                    return State.Hover;
                break;

            //Rotating selected
            case State.RotateSelected:
                mouseCursor = MouseCursor.RotateArrow;
                RotateSelected();
                if ( TryStopDrag() )
                    return State.Hover;
                break;

            //Scaling selected
            case State.ScaleSelected:
                mouseCursor = MouseCursor.ScaleArrow;
                ScaleSelected();
                if ( TryStopDrag() )
                    return State.Hover;
                break;

            //Extruding
            case State.Extrude:
                mouseCursor = MouseCursor.MoveArrow;
                MoveSelected( mousePosition - clickPosition );
                if ( doExtrudeUpdate && mousePosition != clickPosition )
                {
                    UpdatePoly( false, false );
                    doExtrudeUpdate = false;
                }
                if ( TryStopDrag() )
                    return State.Hover;
                break;
        }
        return state;
    }

    void UpdatePoly( bool sizeChanged, bool recordUndo )
    {
        if ( sizeChanged )
        {
            polygon.keyPoints = new List<Vector3>( keyPoints );
        }
        else
        {
            for ( int i = 0; i < keyPoints.Count; i++ )
            {
                polygon.keyPoints[i] = keyPoints[i];
            }
        }
        polygon.BuildMesh();
    }

    void MoveKeyPoint( int index, Vector3 amount )
    {
       keyPoints[index] = polygon.keyPoints[index] + amount;
    }

    void MoveSelected( Vector3 amount )
    {
        foreach ( var i in selectedIndices )
            MoveKeyPoint( i, amount );
    }

    void RotateSelected()
    {
        var center = GetSelectionCenter();

        Handles.color = Color.black;
        Handles.DrawLine( center, clickPosition );
        Handles.color = Color.green;
        Handles.DrawLine( center, mousePosition );

        var clickOffset = clickPosition - center;
        var mouseOffset = mousePosition - center;
        var clickAngle = Mathf.Atan2( clickOffset.y, clickOffset.x );
        var mouseAngle = Mathf.Atan2( mouseOffset.y, mouseOffset.x );
        var angleOffset = mouseAngle - clickAngle;

        foreach ( var i in selectedIndices )
        {
            var point = polygon.keyPoints[i];
            var pointOffset = point - center;
            var a = Mathf.Atan2( pointOffset.y, pointOffset.x ) + angleOffset;
            var d = pointOffset.magnitude;
            keyPoints[i] = center + new Vector3( Mathf.Cos( a ) * d, Mathf.Sin( a ) * d );
        }
    }

    void ScaleSelected()
    {
        Handles.color = Color.green;
        Handles.DrawLine( clickPosition, mousePosition );

        var center = GetSelectionCenter();
        var scale = mousePosition - clickPosition;

        //Uniform scaling if shift pressed
        if ( Event.current.shift )
        {
            if ( Mathf.Abs( scale.x ) > Mathf.Abs( scale.y ) )
                scale.y = scale.x;
            else
                scale.x = scale.y;
        }

        //Determine direction of scaling
        if ( scale.x < 0 )
            scale.x = 1 / (-scale.x + 1);
        else
            scale.x = 1 + scale.x;
        if ( scale.y < 0 )
            scale.y = 1 / (-scale.y + 1);
        else
            scale.y = 1 + scale.y;

        foreach ( var i in selectedIndices )
        {
            var point = polygon.keyPoints[i];
            var offset = point - center;
            offset.x *= scale.x;
            offset.y *= scale.y;
            keyPoints[i] = center + offset;
        }
    }
    #endregion

    #region State Checking

    bool TryHoverKeyPoint( out int index )
    {
        if ( TryHover( keyPoints, Color.black, out index ) )
        {
            mouseCursor = MouseCursor.MoveArrow;
            return true;
        }
        return false;
    }

    bool TryDragKeyPoint( int index )
    {
        if ( TryDrag( keyPoints, index ) )
        {
            return true;
        }
        return false;
    }

    bool TryHover( List<Vector3> points, Color color, out int index )
    {
        if ( Tools.current == Tool.Move )
        {
            index = NearestPoint( points );
            if ( index >= 0 && IsHovering( points[index] ) )
            {
                Handles.color = color;
                DrawCircle( points[index], clickRadius );
                return true;
            }
        }
        index = -1;
        return false;
    }

    bool TryDrag( List<Vector3> points, int index )
    {
        if ( Event.current.type == EventType.MouseDown && IsHovering( points[index] ) )
        {
            clickPosition = mousePosition;
            return true;
        }
        return false;
    }

    bool TryStopDrag()
    {
        if ( Event.current.type == EventType.MouseUp )
        {
            dragIndex = -1;
            UpdatePoly( false, state != State.Extrude );
            return true;
        }
        return false;
    }

    bool TryBoxSelect()
    {
        if ( Event.current.type == EventType.MouseDown )
        {
            clickPosition = mousePosition;
            return true;
        }
        return false;
    }

    bool TryBoxSelectEnd()
    {
        var min = new Vector3( Mathf.Min( clickPosition.x, mousePosition.x ), Mathf.Min( clickPosition.y, mousePosition.y ) );
        var max = new Vector3( Mathf.Max( clickPosition.x, mousePosition.x ), Mathf.Max( clickPosition.y, mousePosition.y ) );
        Handles.color = Color.black;
        Handles.DrawLine( new Vector3( min.x, min.y ), new Vector3( max.x, min.y ) );
        Handles.DrawLine( new Vector3( min.x, max.y ), new Vector3( max.x, max.y ) );
        Handles.DrawLine( new Vector3( min.x, min.y ), new Vector3( min.x, max.y ) );
        Handles.DrawLine( new Vector3( max.x, min.y ), new Vector3( max.x, max.y ) );

        if ( Event.current.type == EventType.MouseUp )
        {
            var rect = new Rect( min.x, min.y, max.x - min.x, max.y - min.y );

            if ( !control )
                selectedIndices.Clear();
            for ( int i = 0; i < keyPoints.Count; i++ )
                if ( rect.Contains( keyPoints[i] ) )
                    selectedIndices.Add( i );

            return true;
        }
        return false;
    }

    bool TryDragSelected()
    {
        if ( selectedIndices.Count > 0 && TryDragButton( GetSelectionCenter(), 0.2f ) )
        {
            clickPosition = mousePosition;
            return true;
        }
        return false;
    }

    bool TryRotateSelected()
    {
        if ( selectedIndices.Count > 0 && TryRotateButton( GetSelectionCenter(), 0.3f ) )
        {
            clickPosition = mousePosition;
            return true;
        }
        return false;
    }

    bool TryScaleSelected()
    {
        if ( selectedIndices.Count > 0 && TryScaleButton( GetSelectionCenter(), 0.3f ) )
        {
            clickPosition = mousePosition;
            return true;
        }
        return false;
    }

    bool TryDragButton( Vector3 position, float size )
    {
        size *= HandleUtility.GetHandleSize( position );
        if ( Vector3.Distance( mousePosition, position ) < size )
        {
            if ( Event.current.type == EventType.MouseDown )
                return true;
            else
            {
                mouseCursor = MouseCursor.MoveArrow;
                Handles.color = Color.blue;
            }
        }
        else
            Handles.color = Color.black;
        var buffer = size / 2;
        Handles.DrawLine( new Vector3( position.x - buffer, position.y ), new Vector3( position.x + buffer, position.y ) );
        Handles.DrawLine( new Vector3( position.x, position.y - buffer ), new Vector3( position.x, position.y + buffer ) );
        Handles.RectangleCap( 0, position, Quaternion.identity, size );
        return false;
    }

    bool TryRotateButton( Vector3 position, float size )
    {
        size *= HandleUtility.GetHandleSize( position );
        var dist = Vector3.Distance( mousePosition, position );
        var buffer = size / 4;
        if ( dist < size + buffer && dist > size - buffer )
        {
            if ( Event.current.type == EventType.MouseDown )
                return true;
            else
            {
                mouseCursor = MouseCursor.RotateArrow;
                Handles.color = Color.blue;
            }
        }
        else
            Handles.color = Color.black;
        Handles.CircleCap( 0, position, inverseRotation, size - buffer / 2 );
        Handles.CircleCap( 0, position, inverseRotation, size + buffer / 2 );
        return false;
    }

    bool TryScaleButton( Vector3 position, float size )
    {
        size *= HandleUtility.GetHandleSize( position );
        if ( Vector3.Distance( mousePosition, position ) < size )
        {
            if ( Event.current.type == EventType.MouseDown )
                return true;
            else
            {
                mouseCursor = MouseCursor.ScaleArrow;
                Handles.color = Color.blue;
            }
        }
        else
            Handles.color = Color.black;
        var buffer = size / 4;
        Handles.DrawLine( new Vector3( position.x - size - buffer, position.y ), new Vector3( position.x - size + buffer, position.y ) );
        Handles.DrawLine( new Vector3( position.x + size - buffer, position.y ), new Vector3( position.x + size + buffer, position.y ) );
        Handles.DrawLine( new Vector3( position.x, position.y - size - buffer ), new Vector3( position.x, position.y - size + buffer ) );
        Handles.DrawLine( new Vector3( position.x, position.y + size - buffer ), new Vector3( position.x, position.y + size + buffer ) );
        Handles.RectangleCap( 0, position, Quaternion.identity, size );
        return false;
    }

    bool TrySelectAll()
    {
        if ( KeyPressed( selectAllKey ) )
        {
            selectedIndices.Clear();
            for ( int i = 0; i < keyPoints.Count; i++ )
                selectedIndices.Add( i );
            return true;
        }
        return false;
    }

    bool TrySplitLine()
    {
        if ( nearestLine >= 0 && KeyPressed( splitKey ) )
        {
            if ( nearestLine == keyPoints.Count - 1 )
            {
                keyPoints.Add( splitPosition );
            }
            else
            {
                keyPoints.Insert( nearestLine + 1, splitPosition );
            }
            UpdatePoly( true, true );
            return true;
        }
        return false;
    }

    bool TryExtrude()
    {
        if ( nearestLine >= 0 && extrudeKeyDown && Event.current.type == EventType.MouseDown )
        {
            var a = nearestLine;
            var b = (nearestLine + 1) % keyPoints.Count;
            if ( b == 0 && a == keyPoints.Count - 1 )
            {
                //Extrude between the first and last points
                keyPoints.Add( polygon.keyPoints[a] );
                keyPoints.Add( polygon.keyPoints[b] );

                selectedIndices.Clear();
                selectedIndices.Add( keyPoints.Count - 2 );
                selectedIndices.Add( keyPoints.Count - 1 );
            }
            else
            {
                //Extrude between two inner points
                var pointA = keyPoints[a];
                var pointB = keyPoints[b];
                keyPoints.Insert( a + 1, pointA );
                keyPoints.Insert( a + 2, pointB );

                selectedIndices.Clear();
                selectedIndices.Add( a + 1 );
                selectedIndices.Add( a + 2 );
            }

            clickPosition = mousePosition;
            doExtrudeUpdate = true;
            UpdatePoly( true, true );
            return true;
        }
        return false;
    }

    bool TryDeleteSelected()
    {
        if ( KeyPressed( KeyCode.Backspace ) || KeyPressed( KeyCode.Delete ) )
        {
            if ( selectedIndices.Count > 0 )
            {
                if ( keyPoints.Count - selectedIndices.Count >= 3 )
                {
                    for ( int i = selectedIndices.Count - 1; i >= 0; i-- )
                    {
                        var index = selectedIndices[i];
                        keyPoints.RemoveAt( index );
                    }
                    selectedIndices.Clear();
                    UpdatePoly( true, true );
                    return true;
                }
            }
        }
        return false;
    }

    bool IsHovering( Vector3 point )
    {
        return Vector3.Distance( mousePosition, point ) < HandleUtility.GetHandleSize( point ) * clickRadius;
    }

    int NearestPoint( List<Vector3> points )
    {
        var near = -1;
        var nearDist = float.MaxValue;
        for ( int i = 0; i < points.Count; i++ )
        {
            var dist = Vector3.Distance( points[i], mousePosition );
            if ( dist < nearDist )
            {
                nearDist = dist;
                near = i;
            }
        }
        return near;
    }

    int NearestLine( out Vector3 position )
    {
        var near = -1;
        var nearDist = float.MaxValue;
        position = keyPoints[0];
        var linePos = Vector3.zero;
        for ( int i = 0; i < keyPoints.Count; i++ )
        {
            var j = (i + 1) % keyPoints.Count;
            var line = keyPoints[j] - keyPoints[i];
            var offset = mousePosition - keyPoints[i];
            var dot = Vector3.Dot( line.normalized, offset );
            if ( dot >= 0 && dot <= line.magnitude )
            {
                linePos = keyPoints[i] + line.normalized * dot;
                var dist = Vector3.Distance( linePos, mousePosition );
                if ( dist < nearDist )
                {
                    nearDist = dist;
                    position = linePos;
                    near = i;
                }
            }
        }
        return near;
    }

    bool KeyPressed( KeyCode key )
    {
        return Event.current.type == EventType.KeyDown && Event.current.keyCode == key;
    }

    bool KeyReleased( KeyCode key )
    {
        return Event.current.type == EventType.KeyUp && Event.current.keyCode == key;
    }

    Vector3 GetSelectionCenter()
    {
        var center = Vector3.zero;
        foreach ( var i in selectedIndices )
            center += polygon.keyPoints[i];
        return center / selectedIndices.Count;
    }

    #endregion

    #region Drawing

    void DrawAxis()
    {
        Handles.color = Color.red;
        var size = HandleUtility.GetHandleSize( Vector3.zero ) * 0.1f;
        Handles.DrawLine( new Vector3( -size, 0 ), new Vector3( size, 0 ) );
        Handles.DrawLine( new Vector3( 0, -size ), new Vector2( 0, size ) );
    }

    void DrawKeyPoint( int index )
    {
        Handles.DotCap( 0, keyPoints[index], Quaternion.identity, HandleUtility.GetHandleSize( keyPoints[index] ) * 0.03f );
    }

    void DrawSegment( int index )
    {
        var from = keyPoints[index];
        var to = keyPoints[(index + 1) % keyPoints.Count];
        Handles.DrawLine( from, to );
    }

    void DrawCircle( Vector3 position, float size )
    {
        Handles.CircleCap( 0, position, inverseRotation, HandleUtility.GetHandleSize( position ) * size );
    }

    void DrawNearestLineAndSplit()
    {
        if ( nearestLine >= 0 )
        {
            Handles.color = Color.blue;
            DrawSegment( nearestLine );
            Handles.color = Color.black;
            Handles.DotCap( 0, splitPosition, Quaternion.identity, HandleUtility.GetHandleSize( splitPosition ) * 0.03f );
        }
    }

    #endregion

    #region Menu Items

    [MenuItem( "GameObject/2D Object/Polygon", false, 1000 )]
    static void CreatePolyMesh()
    {
        var obj = new GameObject( "Polygon", typeof( MeshFilter ), typeof( MeshRenderer ), typeof( PolygonCollider2D ) );
        var polygon = obj.AddComponent<Polygon2D>();
        CreateSquare( polygon, 0.5f );
    }

    static void CreateSquare( Polygon2D polygon, float size )
    {
        polygon.keyPoints.AddRange( new Vector3[] { new Vector3( size, size ), new Vector3( size, -size ), new Vector3( -size, -size ), new Vector3( -size, size ) } );
        polygon.BuildMesh();
    }

    #endregion
}

