﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor( typeof( TextMesh ) )]
public class TextMeshEditor : Editor
{
    public override void OnInspectorGUI()
    {
        this.DrawDefaultInspector();
        TextMesh current_target = target as TextMesh;

        EditorGUILayout.LabelField( "Text:", "" );

        current_target.text = EditorGUILayout.TextArea( current_target.text, GUILayout.MaxHeight( 300f ) );
    }
}
