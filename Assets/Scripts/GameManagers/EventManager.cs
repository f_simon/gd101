﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EventManager : MonoBehaviour {
	private Dictionary<string, MethodEntry> methods = new Dictionary<string,MethodEntry>();

	private static EventManager instance;
	public static EventManager GetInstance()
	{
		if ( instance != null )
			return instance;

		instance = GameObject.FindObjectOfType<EventManager>();
		if ( instance != null )
			return instance;

		GameObject obj = new GameObject( "EventManager" );
		instance = obj.AddComponent<EventManager>();
		return instance;
	}

	public void PutMethodEntry( MethodEntry entry )
	{
		methods.Add( entry.MethodName, entry );
	}

	public string[] GetAllMethodNames()
	{
		string[] keys = new string[methods.Keys.Count];
		methods.Keys.CopyTo( keys, 0 );

		return keys;
	}

	public MethodEntry GetMethodEntry( string methodName )
	{
		return methods[methodName];
	}

}
