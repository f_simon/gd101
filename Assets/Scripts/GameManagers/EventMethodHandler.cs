﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class EventMethodHandler : MonoBehaviour {
	private static EventMethodHandler instance;
	private EventManager eventManager;
	public static EventMethodHandler GetInstance()
	{
		if ( instance != null )
			return instance;

		instance = GameObject.FindObjectOfType<EventMethodHandler>();
		if ( instance != null )
			return instance;

		GameObject obj = new GameObject( "EventManager" );
		instance = obj.AddComponent<EventMethodHandler>();
		return instance;
	}

	void Start () {
		RegisterMethods();
	}

	public void RegisterMethods()
	{
		if( eventManager == null )
		{
			eventManager = GetComponent<EventManager>();
			if ( eventManager == null )
				eventManager = gameObject.AddComponent<EventManager>();
		}

		TestCameraMovement camera = GameObject.FindObjectOfType<TestCameraMovement>();
		if ( camera != null )
		{
			MethodEntry methodEntry = new MethodEntry( "TestMethod", new TriggerMethod<int>( camera.TestMethod ) );
			eventManager.PutMethodEntry( methodEntry );

			methodEntry = new MethodEntry( "SetFollowTargetVertically", new TriggerMethod<bool>( camera.SetFollowTargetVertically ) );
			eventManager.PutMethodEntry( methodEntry );

			methodEntry = new MethodEntry( "DisableFollowTargetVertically", new TriggerMethod( camera.DisableFollowTargetVertically ) );
			eventManager.PutMethodEntry( methodEntry );
		}

		EnergyController energyController = GameObject.FindObjectOfType<EnergyController>();
		if( energyController != null )
		{
			MethodEntry methodEntry = new MethodEntry( "SetEnergyLosingFactor", new TriggerMethod<int>( energyController.SetEnergyLosingFactor ) );
			eventManager.PutMethodEntry( methodEntry );
		}
	}
}
