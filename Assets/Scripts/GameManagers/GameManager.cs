﻿using System.IO;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour
{
    private LightManager lightManager;
    private Text gameOverText;
    private Text randomText;
    private Image brightnessImage;
    private bool isFinished;

    private Image hudBackground;
    private Text energyText;

    private static GameManager instance;
    public static GameManager GetInstance()
    {
        if ( instance != null )
            return instance;

        instance = GameObject.FindObjectOfType<GameManager>();
        if ( instance != null )
            return instance;

        GameObject obj = new GameObject( "GameManager" );
        instance = obj.AddComponent<GameManager>();
        return instance;
    }

    void Awake()
    {
        lightManager = LightManager.GetInstance();

        brightnessImage = UIGenerator.GenerateImage( "BrightnessImage" );
        brightnessImage.color = new Color( 1.0f, 1.0f, 1.0f, 0.0f );

        gameOverText = UIGenerator.GenerateText( "GameOverText", true );

        randomText = UIGenerator.GenerateText( "RandomText", false );
        ResetRandomText();

        GernerateHUD();
    }

    void Update()
    {
        if ( Input.GetButtonDown( "Restart" ) )
            Application.LoadLevel( Application.loadedLevel );
    }

    public bool IsGameFinished()
    {
        return isFinished;
    }

    public void GameIsOver()
    {
        isFinished = true;
        StartCoroutine( CoroutineGameIsOver() );
    }

    public void changeEnergyTextColor( Color color )
    {
        energyText.color = color;
    }

    private IEnumerator CoroutineGameIsOver()
    {
        // prepare Game Over state
        gameOverText.text = "Game Over";
        gameOverText.color = new Color( 1.0f, 0.0f, 0.0f, 0.0f );

        // Turn the lights of
        lightManager.FadeToState( 0.0f, 1.0f );

        // Fade out HUD
        StartCoroutine( FadeTextAlphaTo( energyText, 0.0f, 0.5f ) );
        StartCoroutine( FadeImageAlphaTo( hudBackground, 0.0f, 0.5f ) );
        yield return new WaitForSeconds( 0.5f );
        
        // Fade in some snoozing texts
        StartCoroutine( FadeTextSizeTo( randomText, 150, 1.0f ) );
        yield return new WaitForSeconds( 1.5f );

        ResetRandomText();
        StartCoroutine( FadeTextSizeTo( randomText, 150, 1.0f ) );
        yield return new WaitForSeconds( 1.5f );

        ResetRandomText();
        StartCoroutine( FadeTextSizeTo( randomText, 150, 1.0f ) );
        yield return new WaitForSeconds( 1.1f );
       
        // show GameOverText
        randomText.fontSize = 0;
        StartCoroutine( FadeTextAlphaTo( gameOverText, 1.0f, 2.0f ) );
    }

    public void GameIsSucceeded()
    {
        // Fade out HUD
        StartCoroutine( FadeTextAlphaTo( energyText, 0.0f, 0.5f ) );
        StartCoroutine( FadeImageAlphaTo( hudBackground, 0.0f, 0.5f ) );

        isFinished = true;
        gameOverText.text = "Geschafft";
        gameOverText.color = new Color( 0.0f, 1.0f, 0.0f, 0.0f );
        lightManager.FadeToState( 1.0f, 1.0f );
        StartCoroutine(FadeImageAlphaTo(brightnessImage, 1.1f, 1.0f));
        StartCoroutine( FadeTextAlphaTo( gameOverText, 1.0f, 1.0f ) );
    }

    private IEnumerator FadeTextSizeTo( Text textElement, float size, float duration )
    {
		textElement.enabled = true;
        float start = textElement.fontSize;
        for ( float t = 0.0f; t <= 1.0f; t += Time.deltaTime / duration )
        {
            textElement.fontSize = (int)(Mathf.SmoothStep(start, size, t));
            yield return null;
        }

    }

    private IEnumerator FadeTextAlphaTo( Text textElement, float alphaValue, float duration )
    {
        float start = textElement.color.a;
        Color newColor = textElement.color;
        for( float t = 0.0f; t <= 1.0f; t += Time.deltaTime / duration)
        {
            newColor.a = Mathf.Lerp(start, alphaValue, t);
            textElement.color = newColor;
            yield return null;
        }
        newColor.a = alphaValue;
        textElement.color = newColor;
        yield return null;
    }

    private IEnumerator FadeImageAlphaTo( Image imageElement, float alphaValue, float duration )
    {
        float start = imageElement.color.a;
        Color newColor = imageElement.color;
        for ( float t = 0.0f; t <= 1.0f; t += Time.deltaTime / duration )
        {
            newColor.a = Mathf.Lerp( start, alphaValue, t );
            imageElement.color = newColor;
            yield return null;
        }

        newColor.a = alphaValue;
        imageElement.color = newColor;
        yield return null;
    }

    private void ResetRandomText()
    {
        float left = Random.Range( 0.2f, 0.8f );
        float bottom = Random.Range( 0.2f, 0.8f );
        float zRot = Random.Range( -40.0f, 40.0f );

        randomText.rectTransform.sizeDelta = new Vector2( 300, 200 );
        randomText.rectTransform.anchorMin = new Vector2( left, bottom );
        randomText.rectTransform.anchorMax = new Vector2( left, bottom );
        randomText.rectTransform.Rotate( 0.0f, 0.0f, zRot );

        randomText.text = "Zzz";
        randomText.fontSize = 0;
		randomText.enabled = false;
    }

    private void GernerateHUD()
    {
        hudBackground = UIGenerator.GenerateImage( "HUDBackground" );
        hudBackground.rectTransform.anchorMin = new Vector2( 0.0f, 0.81f );
        hudBackground.rectTransform.anchorMax = new Vector2( 0.3f, 1.0f );
        hudBackground.rectTransform.Rotate( 0.0f, 0.0f, 6.0f );
        hudBackground.rectTransform.offsetMin = new Vector2( -10, 0 );
        hudBackground.rectTransform.offsetMax = new Vector2( 0, 15 );

        Shadow shadowComponent = hudBackground.gameObject.AddComponent<Shadow>();
        shadowComponent.effectDistance = new Vector2( 3.0f, -3.5f );

        energyText = UIGenerator.GenerateText( "EnergyText", true );
        energyText.rectTransform.anchorMin = new Vector2( 0, 0.8f );
        energyText.rectTransform.anchorMax = new Vector2( 0.3f, 1.0f );
        energyText.rectTransform.localScale = new Vector3( 0.7f, 0.7f, 0.7f );
        energyText.rectTransform.Rotate( 0.0f, 0.0f, 6.0f );
        energyText.resizeTextMinSize = 20;
        energyText.resizeTextMaxSize = 200;
        energyText.color = Color.black;
        energyText.alignment = TextAnchor.MiddleLeft;
        energyText.text = "Energy: 20";

    }

    public void AdjustEnergyHUD( int energyValue )
    {
        if( energyText != null )
            energyText.text = "Energy: " + energyValue;
    }

}
