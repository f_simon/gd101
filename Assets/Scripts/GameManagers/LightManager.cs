﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LightManager : MonoBehaviour {

    private Light environmentLight;
    private Light playerLight;
    private Image darknessModificator;

    public float lightAnimationSpeed = 0.07f;

	[Range(0.0f, 1.0f)]
    public float lightState = 1.0f;

	private Color color;

	private const float minEnv = 0.0f;
	private const float maxEnv = 0.45f;

	private const float minPl = 0.0f;
	private const float maxPl = 1.0f;

	private const float minDark = 0.0f;
	private const float stateChangeDark = 0.4f;
	private const float maxDark = 1.0f;

	private float startLightState = 1.0f;
    [Range( 0.0f, 1.0f )]
	public float targetLightState = 1.0f;
	private float time = 0.0f;
	private float speed = 0.0f;
	private bool isFading = false;

    private static LightManager instance;
    public static LightManager GetInstance()
    {
        if ( instance != null )
            return instance;

        instance = GameObject.FindObjectOfType<LightManager>();
        if ( instance != null )
            return instance;

        GameObject obj = new GameObject( "LightManager" );
        instance = obj.AddComponent<LightManager>();
        return instance;
    }

	void Start () {
        // create directional light if necessary
        if( environmentLight == null )
        {
            // go through all existing lights and have a look, if there is an environment light
            bool found = false;
            Light[] lights = GameObject.FindObjectsOfType<Light>();
            foreach( Light light in lights )
            {
                if( light.type == LightType.Directional )
                {
                    environmentLight = light;
                    found = true;
                    break;
                }
            }

            // no environment light was found, so we create one.
            if( !found )
            {
                GameObject lightObject = new GameObject( "EnvironmentLight" );
                lightObject.transform.position = new Vector3( 0, 0, -20.0f );
                environmentLight = lightObject.AddComponent<Light>();
                environmentLight.type = LightType.Directional;
            }
        }

        if( playerLight == null )
        {
            GameObject player = GameObject.FindGameObjectWithTag( "Player" );
            if( player != null )
            {
                playerLight = player.GetComponentInChildren<Light>();
                if( playerLight == null )
                {
                    GameObject lightObject = new GameObject( "PlayerLight" );
                    lightObject.transform.parent = player.transform;
                    lightObject.transform.position = new Vector3( 0, 0, -3 );

                    playerLight = lightObject.AddComponent<Light>();
                    playerLight.type = LightType.Point;
                    playerLight.range = 20;
                }
            }
            else
            {
                Debug.LogError( "LightManager: There exists no player object. Check your scene." );


                GameObject lightObject = new GameObject( "PlayerLight" );
                lightObject.transform.position = new Vector3( 0, 0, -3 );

                playerLight = lightObject.AddComponent<Light>();
                playerLight.type = LightType.Point;
                playerLight.range = 20;
            }
        }

        // create darkness UI
        darknessModificator = UIGenerator.GenerateImage( "DarknessModificator" );
        darknessModificator.rectTransform.SetAsFirstSibling();
        darknessModificator.color = new Color( 0.0f, 0.0f, 0.0f, 0.0f );
		color = darknessModificator.color;
	}
	
	void Update () {
		// handle animations step
        if ( time > 1.0f && isFading )
        {
            lightState = targetLightState;
            isFading = false;
        }

        //Debug.Log( "lightState: " + lightState + ", target: " + targetLightState + ", dt: " + Time.deltaTime );

        if ( !isFading && Mathf.Abs( targetLightState - lightState ) < lightAnimationSpeed * Time.deltaTime )
            lightState = targetLightState;

		if( isFading )
        {
            // a fading call exists and the light manager fades to specific value
			time += Time.deltaTime * speed;
			lightState = Mathf.Lerp(startLightState, targetLightState, time);
		}
        else if ( !isFading && Mathf.Abs( targetLightState - lightState ) > Time.deltaTime )
        {
            // no fading call exists, but there is a different targetState and so the light manager tries to fade to this target state
            lightState += Mathf.Sign( targetLightState - lightState ) * Time.deltaTime * lightAnimationSpeed;
        }

		/* two basic states: 
		 *		lightState > 0.2: interpolate between min and max values
		 *		lightState < 0.2: switch off player light and maximize darkness => lightState == 0.0 <=> complete darkness, player is sleeping
		 */
		if (lightState > 0.2f)
		{
			float interpolationValue = (lightState - 0.2f)*(1.0f/0.8f);

			playerLight.intensity = Mathf.SmoothStep(minPl, maxPl, 1 - interpolationValue);
			environmentLight.intensity = Mathf.SmoothStep(minEnv, maxEnv, interpolationValue);
			color.a = Mathf.SmoothStep(minDark, maxDark - (1.0f - stateChangeDark), 1 - interpolationValue);
			darknessModificator.color = color;
		}
		else
		{
			float interpolationValue = lightState * 5;
			playerLight.intensity = Mathf.SmoothStep(minPl, maxPl, interpolationValue);
			environmentLight.intensity = 0.0f;
			color.a = Mathf.SmoothStep(minDark + stateChangeDark, maxDark, 1 - interpolationValue);
			darknessModificator.color = color;
		}
	}

	public void FadeToState( float targetState, float duration = 1.0f )
	{
		if (duration <= Mathf.Epsilon || duration < 0.0f)
			duration = 1.0f;

		this.speed = 1.0f / duration;
		this.isFading = true;
		this.startLightState = this.lightState;
		this.targetLightState = targetState;
		this.time = 0.0f;
	}

    public void SetState( float state )
    {
        this.isFading = false;
        this.targetLightState = state;
    }
}
