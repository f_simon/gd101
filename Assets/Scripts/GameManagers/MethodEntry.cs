﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public delegate void TriggerMethod<T>( T arg );
public delegate void TriggerMethod();

/** this class represents a method entry in the event manager. It contains its method name, its parameter type and a delegate to the method that should be run.*/
[System.Serializable]
public class MethodEntry
{
	#region Variables
	[SerializeField]
	private string methodName;

	[SerializeField]
	private ParameterType parameterType;

	// Method delegates
	private TriggerMethod<int> intMethod;
	private TriggerMethod<bool> boolMethod;
	private TriggerMethod<string> stringMethod;
	private TriggerMethod<float> floatMethod;
	private TriggerMethod voidMethod;
	#endregion

	#region Properties
	public string MethodName
	{
		get { return methodName; }
	}

	public ParameterType ParamType
	{
		get { return parameterType;  }
	}

	public TriggerMethod<int> IntMethod
	{
		get
		{
			if ( parameterType != ParameterType.Int )
				throw new System.InvalidOperationException( "Wrong method was read. Type is " + parameterType + ", but used Int" );
			return intMethod;
		}
	}

	public TriggerMethod<float> FloatMethod
	{
		get
		{
			if ( parameterType != ParameterType.Float )
				throw new System.InvalidOperationException( "Wrong method was read. Type is " + parameterType + ", but used Float" );
			return floatMethod;
		}
	}

	public TriggerMethod<string> StringMethod
	{
		get
		{
			if ( parameterType != ParameterType.String )
				throw new System.InvalidOperationException( "Wrong method was read. Type is " + parameterType + ", but used String" );
			return stringMethod;
		}
	}

	public TriggerMethod<bool> BoolMethod
	{
		get
		{
			if ( parameterType != ParameterType.Bool )
				throw new System.InvalidOperationException( "Wrong method was read. Type is " + parameterType + ", but used Bool" );
			return boolMethod;
		}
	}

	public TriggerMethod VoidMethod
	{
		get
		{
			if ( parameterType != ParameterType.None )
				throw new System.InvalidOperationException( "Wrong method was read. Type is " + parameterType + ", but used Void" );
			return voidMethod;
		}
	}

	#endregion

	#region Constructors
	public MethodEntry( string methodName, TriggerMethod<int> method )
	{
		this.methodName = methodName;
		this.parameterType = ParameterType.Int;
		this.intMethod = method;
	}

	public MethodEntry( string methodName, TriggerMethod<float> method )
	{
		this.methodName = methodName;
		this.parameterType = ParameterType.Float;
		this.floatMethod = method;
	}

	public MethodEntry( string methodName, TriggerMethod<string> method )
	{
		this.methodName = methodName;
		this.parameterType = ParameterType.String;
		this.stringMethod = method;
	}

	public MethodEntry( string methodName, TriggerMethod<bool> method )
	{
		this.methodName = methodName;
		this.parameterType = ParameterType.Bool;
		this.boolMethod = method;
	}

	public MethodEntry( string methodName, TriggerMethod method )
	{
		this.methodName = methodName;
		this.parameterType = ParameterType.None;
		this.voidMethod = method;
	}
	#endregion

}

/** this class represents a parameter entry within an EventTrigger. 
 * It is used to set the parameter values for a certain method. 
 * It contains the name of the method that should be called, the parameter type and a variable for the parameter.
 * The basic idea is that you can get the method delegat that should be called by the method name from the event manager.
 * This workaround is necessary since it is quite difficult to serialize delegates and therefore the event manager provides
 * a simple solution that comes without serialization.
 */ 
[System.Serializable]
public class ParameterEntry
{
	#region Variables
	[SerializeField]
	private string methodName;

	[SerializeField]
	private ParameterType parameterType;

	// Parameter Values
	[SerializeField]
	private int intParameter;
	[SerializeField]
	private bool boolParameter;
	[SerializeField]
	private string stringParameter;
	[SerializeField]
	private float floatParameter;
	#endregion

	#region Properties
	public string MethodName
	{
		get { return methodName; }
	}

	public ParameterType ParamType
	{
		get { return parameterType; }
	}

	public int IntParameter
	{
		get
		{
			if ( parameterType != ParameterType.Int )
				throw new System.InvalidOperationException( "Wrong parameter value was read. Parameter type is " + parameterType + ", but used Int" );

			return intParameter;
		}
		set { intParameter = value; }
	}

	public float FloatParameter
	{
		get
		{
			if ( parameterType != ParameterType.Float )
				throw new System.InvalidOperationException( "Wrong parameter value was read. Parameter type is " + parameterType + ", but used Float" );
			return floatParameter;
		}
		set { floatParameter = value; }
	}

	public string StringParameter
	{
		get
		{
			if ( parameterType != ParameterType.String )
				throw new System.InvalidOperationException( "Wrong parameter value was read. Parameter type is " + parameterType + ", but used String" );
			return stringParameter;
		}
		set { stringParameter = value; }
	}

	public bool BoolParameter
	{
		get
		{
			if ( parameterType != ParameterType.Bool )
				throw new System.InvalidOperationException( "Wrong parameter value was read. Parameter type is " + parameterType + ", but used Bool" );
			return boolParameter;
		}
		set { boolParameter = value; }
	}

	#endregion

	#region Constructors
	public ParameterEntry( string methodName, ParameterType type )
	{
		this.methodName = methodName;
		this.parameterType = type;
	}


	public ParameterEntry( MethodEntry methodEntry )
	{
		this.methodName = methodEntry.MethodName;
		this.parameterType = methodEntry.ParamType;
	}
	#endregion

}