﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public enum ParameterType
{
	None,
	Int,
	Float,
	String,
	Bool
};

public static class ParameterTypeUtils
{
	public static string GetParamterTypeString( this ParameterType type )
	{
		if ( type == ParameterType.None )
			return "None";
		else if ( type == ParameterType.Int )
			return "Int32";
		else if ( type == ParameterType.Float )
			return "Single";
		else if ( type == ParameterType.String )
			return "String";
		else if ( type == ParameterType.Bool )
			return "Boolean";

		return "None";
	}
}