﻿using UnityEngine;
using System.Collections;

public class ValueManager : MonoBehaviour 
{
	public int ENEMY_DAMAGE_VALUE = -20;
	public float STICK_TIME_BUBBLEGUM = 3.0f;
	public float MAX_INVISIBLE_TIME = 1.0f;

	private static ValueManager instance;
	
	public static ValueManager GetInstance()
	{
		if ( instance != null )
			return instance;

		instance = GameObject.FindObjectOfType<ValueManager>();
		if ( instance != null )
			return instance;

		GameObject obj = new GameObject( "ValueManager" );
		instance = obj.AddComponent<ValueManager>();
		return instance;
	}
}
