﻿using UnityEngine;
using System.Collections;

public class QuitButtonController : MonoBehaviour {

	public bool changeHoverColor = true;
	public Color hoverColor;
	private Color basicColor;

	void Start()
	{
		basicColor = renderer.material.color;
	}

	void OnMouseUp()
	{
		Application.Quit();
	}

	void OnMouseEnter()
	{
		if ( changeHoverColor )
			renderer.material.color = hoverColor;
	}

	void OnMouseExit()
	{
		if ( changeHoverColor )
			renderer.material.color = basicColor;
	}
}
