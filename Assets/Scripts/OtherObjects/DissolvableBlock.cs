﻿using UnityEngine;
using System.Collections;

public class DissolvableBlock : MonoBehaviour {
	public float dissolvingTime;
	private Color materialColor;
	private float currentDissolvingTime;

	void Start()
	{
		materialColor = renderer.material.color;
		currentDissolvingTime = dissolvingTime;
	}

	void OnCollisionStay2D( Collision2D other )
	{
		if (other.gameObject.tag == "Player")
		{
			currentDissolvingTime -= Time.deltaTime;
			if (currentDissolvingTime <= 0.0f)
			{
				Destroy(gameObject);
				return;
			}

			materialColor.a = Mathf.Lerp(1.0f, 0.0f, 1 - currentDissolvingTime / dissolvingTime);
			renderer.material.color = materialColor;
		}
	}
}
