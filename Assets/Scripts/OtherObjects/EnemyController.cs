﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D), typeof(Animator))]
public class EnemyController : MonoBehaviour {
	/* other waypoints where the platform should move to */
	public Transform[] waypoints;
	public float speed;
	public float waitTime;

	private int nextWaypointIndex;
	private bool moving;
	private float currentWaitingTime;

	private Animator animator;
	private bool facingRight = false;

	void Awake()
	{
		if ( waypoints.Length > 0 )
			transform.position = waypoints[0].position;

		animator = GetComponent<Animator>();
	}

	void Start()
	{
		if ( waypoints.Length == 0 )
			return;

		// Add an additional first waypoint at the current position of the game object
		nextWaypointIndex = 0 % waypoints.Length;
	}

	void Update()
	{
		if( !moving )
		{
			currentWaitingTime -= Time.deltaTime;

			if ( currentWaitingTime <= 0.0f )
				moving = true;

			rigidbody2D.velocity = new Vector2( 0.0f, rigidbody2D.velocity.y );
			animator.SetFloat( "Speed", 0 );
		}
	}

	void FixedUpdate()
	{
		if ( waypoints.Length <= 1 )
			return;

		if ( !moving )
			return;

		float direction = Mathf.Sign( (waypoints[nextWaypointIndex].position - transform.position).x );

		rigidbody2D.velocity = new Vector2( speed * direction, rigidbody2D.velocity.y );
		animator.SetFloat( "Speed", speed );

		if ( direction < 0 && facingRight )
			FlipFacing();
		else if ( direction > 0 && !facingRight )
			FlipFacing();

		if ( Vector3.Distance( transform.position, waypoints[nextWaypointIndex].position ) < 0.5f )
		{
			nextWaypointIndex = (nextWaypointIndex + 1) % waypoints.Length;
			currentWaitingTime = waitTime;
			moving = false;
		}
	}

	void FlipFacing()
	{
		facingRight = !facingRight;
		Vector3 scale = transform.localScale;
		scale.x *= -1;
		transform.localScale = scale;
	}
}
