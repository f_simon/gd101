﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SpriteRenderer))]
public class EnergyBubble : MonoBehaviour {
    public Color leftColor;
    public Color rightColor;
    public float colorAnimationSpeed = 0.1f;
    public float zRotationSpeed = 50.0f;

	public int addEnergyValue = 10;

    private SpriteRenderer spriteRenderer;
    private float colorAnimationTime = 0.0f;

    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }


	void Update () {
        transform.Rotate( 0.0f, 0.0f, zRotationSpeed * Time.deltaTime );
        spriteRenderer.color = Color.Lerp( leftColor, rightColor, colorAnimationTime );
        colorAnimationTime += colorAnimationSpeed * Time.deltaTime;
        if ( colorAnimationTime > 1.0f || colorAnimationTime < 0.0f )
            colorAnimationSpeed *= -1;
	}

	void OnTriggerStay2D( Collider2D other )
	{
		if( other.tag == "Player")
        {
			other.SendMessage("AddEnergy", addEnergyValue, SendMessageOptions.DontRequireReceiver );
            Destroy( gameObject );
        }
	}
}
