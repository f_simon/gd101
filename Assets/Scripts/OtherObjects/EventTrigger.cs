﻿using UnityEngine;
using System.Collections;

public enum SelectableTypes
{
	None,
	Int,
	Float,
	String,
	Bool
};

public class EventTrigger : MonoBehaviour {
	public ParameterEntry parameterEntry;

	void OnTriggerEnter2D( Collider2D other )
	{
		// if the player enters the  trigger we will call the corresponing trigger method that is selected for this trigger.
		if(other.tag == "Player")
		{
			switch ( parameterEntry.ParamType )
			{
				case ParameterType.Int:
					EventManager.GetInstance().GetMethodEntry( parameterEntry.MethodName ).IntMethod( parameterEntry.IntParameter );
					break;
				case ParameterType.Float:
					EventManager.GetInstance().GetMethodEntry( parameterEntry.MethodName ).FloatMethod( parameterEntry.FloatParameter );
					break;
				case ParameterType.String:
					EventManager.GetInstance().GetMethodEntry( parameterEntry.MethodName ).StringMethod( parameterEntry.StringParameter );
					break;
				case ParameterType.Bool:
					EventManager.GetInstance().GetMethodEntry( parameterEntry.MethodName ).BoolMethod( parameterEntry.BoolParameter );
					break;
				case ParameterType.None:
					EventManager.GetInstance().GetMethodEntry( parameterEntry.MethodName ).VoidMethod();
					break;
			}
		}
	}

}
