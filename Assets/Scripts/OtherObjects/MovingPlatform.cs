﻿using UnityEngine;
using System.Collections;

public class MovingPlatform : MonoBehaviour {

    /* other waypoints where the platform should move to */
    public Transform[] waypoints;
    public float speed;
	public bool moveOnContact;

    private int nextWaypointIndex;
	private int objectsOnPlatform;
	private bool moving;

	void Awake()
	{
		if ( waypoints.Length > 0 )
			transform.position = waypoints[0].position;
	}

	void Start () {
        // Add an additional first waypoint at the current position of the game object
        nextWaypointIndex = 0 % waypoints.Length;
	}
	
	void FixedUpdate () {
        if ( waypoints.Length <= 1 )
            return;

        transform.position = Vector3.MoveTowards( transform.position, waypoints[nextWaypointIndex].position, speed );

		if ( Vector3.Distance( transform.position, waypoints[nextWaypointIndex].position ) < 0.5f )
		{
			if ( nextWaypointIndex == 0 && moveOnContact && objectsOnPlatform > 0 )
			{
				nextWaypointIndex = (nextWaypointIndex + 1) % waypoints.Length;
				moving = true;
			}
			else if ( nextWaypointIndex > 0 || !moveOnContact  )
			{
				nextWaypointIndex = (nextWaypointIndex + 1) % waypoints.Length;
				moving = true;
			}
			else
			{
				moving = false;
			}
		}
    }

	void OnCollisionEnter2D( Collision2D collision )
	{
		if ( collision.gameObject.transform.position.y < transform.position.y  )
			return;

		collision.gameObject.transform.parent = transform;

		if( collision.gameObject.tag == "Player" )
			objectsOnPlatform++;
	}

	void OnTriggerEnter2D( Collider2D collider )
	{
		// There are some static objects that should be automatically adjusted to the platform 
		// if they are placed on it, so this trigger method is necessary to handle those
		if ( collider.transform.position.y < transform.position.y )
			return;

		collider.gameObject.transform.parent = transform;
	}

	void OnCollisionExit2D( Collision2D collision )
	{
		collision.gameObject.transform.parent = null;

		if ( collision.gameObject.tag == "Player" )
			objectsOnPlatform--;
	}
}
