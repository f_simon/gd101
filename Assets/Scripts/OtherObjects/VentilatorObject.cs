﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))]
public class VentilatorObject : MonoBehaviour {
	public float windForce;
	public Vector3 windDirection;

	void OnTriggerStay2D( Collider2D other )
	{
		if( other.rigidbody2D != null )
		{
			float factor = 10/Vector3.Distance( other.transform.position, transform.position );
			other.rigidbody2D.AddForce( windDirection * windForce * factor );
		}
	}
}
