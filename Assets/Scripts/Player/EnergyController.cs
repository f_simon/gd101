﻿using UnityEngine;
using System.Collections;

public class EnergyController : MonoBehaviour {

    public int maxEnergy = 100;
    public int minEnergy = 0;
    public int initialEnergy = 100;
    public float timeStep = 1.0f;
	public int energyLosingFactor = 1;
    public float timeStepMultiplicator = 3.0f;
    [Range( 0, 1 )]
    public float timeStepMultiplicationBorder = 0.3f;

    private float initialTimeStep;

    private volatile int currentEnergy;
    private GameManager gameManager;
    private LightManager lightManager;

	void Start () 
    {
        if( maxEnergy < minEnergy )
        {
            int tmpEnergy = maxEnergy;
            maxEnergy = minEnergy;
            minEnergy = tmpEnergy;
        }

        if ( maxEnergy < initialEnergy )
            initialEnergy = maxEnergy;

        if ( minEnergy > initialEnergy )
            initialEnergy = minEnergy;

	    currentEnergy = initialEnergy;
        
        initialTimeStep = timeStep;

        gameManager = GameManager.GetInstance();
        lightManager = LightManager.GetInstance();

        float value = (float)(currentEnergy - minEnergy) / (maxEnergy - minEnergy);

        lightManager.SetState( value );

        gameManager.AdjustEnergyHUD( currentEnergy );

        StartCoroutine( UpdateEnergyLevel() );
	}
	
	void Update () 
    {
        if ( gameManager.IsGameFinished() )
            return;

        if ( currentEnergy <= minEnergy )
            gameManager.GameIsOver();

        float value = (float)(currentEnergy - minEnergy) / (maxEnergy - minEnergy);
        lightManager.SetState( value );

        if( value < timeStepMultiplicationBorder )
        {
            timeStep = initialTimeStep * timeStepMultiplicator;
        }
	}

    private IEnumerator UpdateEnergyLevel()
    {
        while( currentEnergy >= minEnergy && !gameManager.IsGameFinished() )
        {
            currentEnergy -= 1 * energyLosingFactor;
            gameManager.AdjustEnergyHUD( currentEnergy );
            yield return new WaitForSeconds( timeStep );
        }
    }

    public void AddEnergy( int value )
    {
        currentEnergy += value;
        if ( currentEnergy > maxEnergy )
            currentEnergy = maxEnergy;

        if ( currentEnergy < minEnergy )
            currentEnergy = minEnergy;
    }

	public void SetEnergyLosingFactor( int factor )
	{
		energyLosingFactor = factor;
	}
}
