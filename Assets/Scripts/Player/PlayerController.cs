﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D), typeof(Animator), typeof(StateController))]
public class PlayerController : MonoBehaviour {

    public float maxSpeed = 10;
    public float jumpForce = 250.0f;

    private bool facingRight = true;
    private Animator animator;
	private StateController stateController;

	// ground check
    private float groundRadius = 0.2f;
    public Transform groundCheckTransform;
    public LayerMask groundMask;

	private GameManager gameManager;
	private ValueManager valueManager;

	/** is the player currently controllable. For example the bubblegum can forbid the player to move */
	private bool controllable = true;

	/** This distant joint is used to bind the player to the bubblegum or other objects that a forcing the player to stick to them */
	private DistanceJoint2D joint;
	// adding a HingeJointComponent that is only used if the player hangs on a rope
	HingeJoint2D ropeHingeJoint;

	/** At the beginning the gravity scale is stored in this variable. If you have to change the gravity scale you can reset it, with this value */
	private float originalGravityScale;

	/** is the trigger currently already activated? It is needed, because the player has two colliders and trigger shall only be entered once*/
	private bool triggered = false;

	private bool visible;
	private float invisibleTime;

	private AudioSource footstepSound;

	void Start () {
        animator = GetComponent<Animator>();
		stateController = GetComponent<StateController>();

        gameManager = GameManager.GetInstance();
		valueManager = ValueManager.GetInstance();

		joint = gameObject.AddComponent<DistanceJoint2D>();
		joint.distance = 0.0f;
		joint.enabled = false;
		joint.collideConnected = true;

		ropeHingeJoint = gameObject.AddComponent<HingeJoint2D>();
		ropeHingeJoint.enabled = false;

		originalGravityScale = rigidbody2D.gravityScale;

		footstepSound = GetComponents<AudioSource>()[0];
	}

	void OnBecameInvisible()
	{
		visible = false;
	}

	void OnBecameVisible()
	{
		visible = true;
		invisibleTime = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {
        
		// is the game still running?
		if ( gameManager == null || gameManager.IsGameFinished() )
            return;

		// may the player be moved?
		if ( !controllable )
			return;

		// if the player gets invisible the game will be over after a short time.
		if( !visible )
		{
			invisibleTime += Time.deltaTime;

			if ( invisibleTime > valueManager.MAX_INVISIBLE_TIME )
			{
				if ( gameManager != null && !gameManager.IsGameFinished() )
				{
					gameManager.GameIsOver();
				}
			}
		}

        if ( stateController.IsMovementAllowed(0.0f) && Input.GetButtonDown( "Jump" ) )
        {
			if( stateController.State == MovementState.OnRope )
			{
				ropeHingeJoint.enabled = false;
				ropeHingeJoint.connectedBody = null;
			}

			Debug.Log("Jump " + stateController.State);

			stateController.State = (MovementState.StartJumping);
            animator.SetBool("Ground", false);
			animator.SetFloat("Speed", 0.0f);
			footstepSound.Stop();
            rigidbody2D.AddForce(new Vector2(0, jumpForce));
        }
	}

    void FixedUpdate()
    {
		// check if movement should be performed
        if ( gameManager == null || gameManager.IsGameFinished() )
            return;

		if (!controllable)
		{
			footstepSound.Stop();
			return;
		}

        // Read horizontal axis input
        float horizontal = Input.GetAxis("Horizontal");

        // do ground check for jumps
        bool grounded = Physics2D.OverlapCircle(groundCheckTransform.position, groundRadius, groundMask);
		if (grounded && stateController.State != MovementState.OnGround && !stateController.IsInState(MovementState.StartJumping, 0.0f) )
		{
			Debug.Log("Set on Ground");
			stateController.State = MovementState.OnGround;
			animator.SetBool("Ground", true);
		}

		if (Mathf.Abs(animator.GetFloat("Speed")) > Mathf.Epsilon && stateController.State == MovementState.OnGround && !footstepSound.isPlaying)
			footstepSound.Play();
		else if (Mathf.Abs(animator.GetFloat("Speed")) < Mathf.Epsilon)
			footstepSound.Stop();		

		
        // if you're jumping from an edge you may tap the jump key a little bit to late and it will be accepted
        if ( !stateController.IsMovementAllowed(0.5f) )
            return;

		if (stateController.IsInState(MovementState.OnGround, 0.5f))
		{
			// Set velocity for the charackter
			rigidbody2D.velocity = new Vector2(horizontal * maxSpeed, rigidbody2D.velocity.y);
			animator.SetFloat("Speed", Mathf.Abs(horizontal));

			// face character in right direction
			if (horizontal > 0 && !facingRight)
			{
				FlipFacing();
			}
			else if (horizontal < 0 && facingRight)
			{
				FlipFacing();
			}
		}
		else if( stateController.IsInState(MovementState.OnRope, 0.5f))
		{
			animator.SetFloat("Speed", 0.0f);
			rigidbody2D.velocity = new Vector2(horizontal * maxSpeed, rigidbody2D.velocity.y);
		}
    }

	void OnTriggerEnter2D( Collider2D other )
	{
		if ( triggered )
			return;

		triggered = true;

		if( other.tag == "Enemy" )
		{
			//Debug.Log( "Enemy collision " + other.name );
			// take damage when hitting an enemy
			gameObject.SendMessage( "AddEnergy", valueManager.ENEMY_DAMAGE_VALUE, SendMessageOptions.DontRequireReceiver );
		}
		else if( other.tag == "Bubblegum")
		{			
			// player is not controllable anymore
			SetControllable( false );

			// attach player to the contacted object with the help of a joint. 
			joint.enabled = true;
			joint.connectedBody = other.rigidbody2D;

			// Reactivate player movement after a short amount of time
			StartCoroutine( SetControllableAfterTime( true, valueManager.STICK_TIME_BUBBLEGUM ) );
		}
		else if( other.tag == "Rope" )
		{
			//Debug.Log("Rope Attached " + stateController.State);
			if (!stateController.IsInState(MovementState.OnRope, 0.5f))
			{
				//transform.position = other.transform.position;
				ropeHingeJoint.connectedBody = other.rigidbody2D;
				ropeHingeJoint.enabled = true;
				stateController.State = MovementState.OnRope;
			}
		}
		else if( other.tag == "ToxicGas")
		{
			gameObject.SendMessage("SetEnergyLosingFactor", 2, SendMessageOptions.DontRequireReceiver);
            gameManager.changeEnergyTextColor( new Color( 0.7f, 0.0f, 0.0f) );
		}
	}

    void OnTriggerStay2D( Collider2D other )
    {
        if ( gameManager == null || gameManager.IsGameFinished() )
            return;

        if( other.tag == "Goal" )
        {
            animator.SetFloat( "Speed", 0.0f );
			footstepSound.Stop();
            gameManager.GameIsSucceeded();
        }
    }

	void OnTriggerExit2D( Collider2D other )
	{
		triggered = false;

		if( other.tag == "ToxicGas")
		{
			gameObject.SendMessage("SetEnergyLosingFactor", 1, SendMessageOptions.DontRequireReceiver);
            gameManager.changeEnergyTextColor(Color.black);
		}
	}

    void FlipFacing()
    {
        facingRight = !facingRight;
        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
    }

	private void SetControllable( bool controllable )
	{
		this.controllable = controllable;
		if ( !controllable )
		{
			// stop animation, don't affect it with gravity, and stop any movement
			animator.SetFloat( "Speed", 0.0f );
			rigidbody2D.gravityScale = 0.0f;
			rigidbody2D.velocity = Vector2.zero;
		}
		else
		{
			// reactivate gravity, disable joint and push object away from bubblegum.
			rigidbody2D.gravityScale = originalGravityScale;
			joint.enabled = false;
			joint.connectedBody = null;
			rigidbody2D.AddForce( new Vector2( 200.0f, 200.0f ) );
		}
	}

	private IEnumerator SetControllableAfterTime( bool controllable, float timeInSeconds )
	{
		yield return new WaitForSeconds( timeInSeconds );
		SetControllable( controllable );
		yield return null;
	}

}
