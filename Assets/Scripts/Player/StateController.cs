﻿using UnityEngine;
using System.Collections;

public enum MovementState
{
	OnGround,
	OnRope,
	InAir,
	StartJumping
};

public class StateController : MonoBehaviour {
	private MovementState state = MovementState.OnGround;
	private MovementState lastState = MovementState.OnGround;

	private float timeSinceStateChange;

	public MovementState State
	{
		get { return state; }
		set 
		{ 
			lastState = state;
			state = value;
			if( state != lastState )
				timeSinceStateChange = 0.0f;
		}
	}

	public void SwitchStateImmediate(MovementState newState)
	{
		lastState = state;
		state = newState;
		if (state != lastState)
			timeSinceStateChange = 10.0f;
	}

	public bool IsInState( MovementState state, float deltaTime = 0.0f )
	{
		if (this.state == state)
			return true;

		if( timeSinceStateChange < deltaTime && lastState == state )
			return true;

		return false;
	}

	public bool IsMovementAllowed( float deltaTime = 0.0f )
	{
		if (this.state == MovementState.OnGround || this.state == MovementState.OnRope)
			return true;

		if (timeSinceStateChange < deltaTime && lastState != MovementState.InAir)
			return true;

		return false;
	}

	void Update()
	{
		timeSinceStateChange += Time.deltaTime;
		if (state == MovementState.StartJumping && timeSinceStateChange > 0.5f)
			State = MovementState.InAir;

		//Debug.Log( "current: " + state + " last: " + lastState + " time: " + timeSinceStateChange);
	}



}
