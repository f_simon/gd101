﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[ExecuteInEditMode, RequireComponent(typeof(MeshFilter))]
public class Polygon2D : MonoBehaviour {
    private static int count;

    public List<Vector3> keyPoints = new List<Vector3>();
    public Vector2 uvPosition;
    public float uvScale = 10;
    public float uvRotation;

    private PolygonCollider2D collider2D;
    private Mesh mesh;

    void Start()
    {
        UpdatePolygonLines( keyPoints.ToArray() );
    }

    public List<Vector3> GetEdgePoints()
    {
        // build list with edge points
        List<Vector3> points = new List<Vector3>();
        for ( int i = 0; i < keyPoints.Count; i++ )
        {
            points.Add( keyPoints[i] );
        }
        return points;
    }

    public void BuildMesh()
    {
        List<Vector3> points = GetEdgePoints();
        Vector3[] vertices = points.ToArray();

        //Build the index array
        List<int> indices = new List<int>();
        while ( indices.Count < points.Count )
            indices.Add( indices.Count );

        //Build the triangle array
        int[] triangles = Triangulator.Points( points );

        //Build the uv array
        float scale = uvScale != 0 ? (1 / uvScale) : 0;
        Matrix4x4 matrix = Matrix4x4.TRS( -uvPosition, Quaternion.Euler( 0, 0, uvRotation ), new Vector3( scale, scale, 1 ) );
        Vector2[] uv = new Vector2[points.Count];
        for ( int i = 0; i < uv.Length; i++ )
        {
            var p = matrix.MultiplyPoint( points[i] );
            uv[i] = new Vector2( p.x, p.y );
        }

        //Find the mesh (create it if it doesn't exist)
        MeshFilter meshFilter = GetComponent<MeshFilter>();
        if ( mesh == null )
        {
            mesh = new Mesh();
            mesh.name = "PolySprite_Mesh_"+(count++);
            meshFilter.mesh = mesh;
        }

        //Update the mesh
        mesh.Clear();
        mesh.vertices = vertices;
        mesh.uv = uv;
        mesh.triangles = triangles;
        mesh.RecalculateNormals();
        mesh.Optimize();

        //Update collider after the mesh is updated
        UpdateCollider( points );

        // Draw polygon lines around our object
        UpdatePolygonLines( vertices );
    }

    private void UpdateCollider( List<Vector3> points )
    {
        // get polygon collider to create 2D collider
        if ( collider2D == null )
            collider2D = GetComponent<PolygonCollider2D>();
       
        // if the collider is still null, then there exists no polygon collider
        if ( collider2D == null )
            return;

        Vector2[] points2D = new Vector2[points.Count];
        for( int i = 0; i < points.Count; ++i )
        {
            Vector3 point = points[i];
            points2D[i] = new Vector2( point.x, point.y );
        }

        collider2D.SetPath( 0, points2D );
    }

    private void UpdatePolygonLines( Vector3[] vertices3D )
    {
        CustomLineRenderer lineRenderer = gameObject.GetComponentInChildren<CustomLineRenderer>();
        if ( lineRenderer == null )
        {
            GameObject lineObject = new GameObject( "LineRenderer", typeof(CustomLineRenderer) );
            lineObject.transform.parent = transform;

            lineRenderer = lineObject.GetComponent<CustomLineRenderer>();
        }

        lineRenderer.SetVertexCount( vertices3D.Length + 1 );
        for ( int i = 0; i <= vertices3D.Length; ++i )
        {
            lineRenderer.SetPosition( i, vertices3D[i % vertices3D.Length] );
            lineRenderer.SetWidth( i, 0.1f );
        }

        lineRenderer.RefreshObject();
    }
}
