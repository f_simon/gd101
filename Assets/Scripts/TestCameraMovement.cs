﻿using UnityEngine;
using System.Collections;

public class TestCameraMovement : MonoBehaviour {

    public Transform horizontalTarget;
	public float verticalSpeed = 1.0f ;
	public bool followTargetVertically = false ;

	private float originalVerticalSpeed;

	// Use this for initialization
	void Start () {
		originalVerticalSpeed = verticalSpeed;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 pos = transform.position;
		if (!followTargetVertically)
			pos.y -= 1.0f * verticalSpeed * Time.deltaTime;
		else
		{
			pos.y = horizontalTarget.position.y;
		}

        if (horizontalTarget != null)
            pos.x = horizontalTarget.position.x;

        transform.position = pos;
	}

	public void EnableFollowTargetVertically()
	{
		followTargetVertically = true;
	}

	public void DisableFollowTargetVertically()
	{
		followTargetVertically = false;
	}

	public void SetFollowTargetVertically( bool follow )
	{
		followTargetVertically = follow;
	}

	public void StopVerticalMovement()
	{
		verticalSpeed = 0.0f;
	}

	public void StartVerticalMovement()
	{
		verticalSpeed = originalVerticalSpeed;
	}

	public void SetVerticalSpeed( float speed )
	{
		verticalSpeed = speed;
		originalVerticalSpeed = verticalSpeed;
	}

	public void TestMethod( int test )
	{
		Debug.Log( "HELLO WORLD " + test );
	}
}
