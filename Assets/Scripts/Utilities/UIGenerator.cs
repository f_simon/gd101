﻿using System;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public static class UIGenerator
{

    private static Canvas canvas;
	private static string fontName = "bobcat";
	private static Font useFont;

    public static Text GenerateText( String name, bool bestFit )
    {
        GenerateCanvas();

        GameObject textObject = new GameObject( name );
        textObject.transform.SetParent( canvas.transform, false );
        textObject.layer = LayerMask.NameToLayer( "UI" );

        Text textElement = textObject.AddComponent<Text>();

        textElement.rectTransform.sizeDelta = Vector2.zero;
        textElement.rectTransform.anchorMin = new Vector2( 0, 0 );
        textElement.rectTransform.anchorMax = new Vector2( 1, 1 );

        textElement.text = "";

		if (useFont == null)
		{
			useFont = Resources.Load<Font>(fontName);
		}

		if(useFont == null )
			useFont = Resources.FindObjectsOfTypeAll<Font>()[0];

        textElement.font = useFont;
        textElement.alignment = TextAnchor.MiddleCenter;

        if ( bestFit )
        {
            textElement.resizeTextForBestFit = true;
            textElement.resizeTextMinSize = 50;
            textElement.resizeTextMaxSize = 200;
        }

        return textElement;
    }

    public static Image GenerateImage(String name)
    {
        GenerateCanvas();

        GameObject imageObject = new GameObject( name );
        imageObject.transform.SetParent( canvas.transform, false );
        imageObject.layer = LayerMask.NameToLayer( "UI" );

        Image imageElement = imageObject.AddComponent<Image>();

        imageElement.rectTransform.sizeDelta = Vector2.zero;
        imageElement.rectTransform.anchorMin = new Vector2( 0, 0 );
        imageElement.rectTransform.anchorMax = new Vector2( 1, 1 );

        imageElement.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);

        return imageElement;
    }

    private static void GenerateCanvas()
    {
        if ( canvas == null )
            canvas = GameObject.FindObjectOfType<Canvas>();

        if ( canvas == null )
        {
            GameObject canvasObject = new GameObject( "Canvas", typeof( RectTransform ) );
            canvasObject.layer = LayerMask.NameToLayer( "UI" );
            canvas = canvasObject.AddComponent<Canvas>();
            CanvasScaler canvasScaler = canvasObject.AddComponent<CanvasScaler>();
            canvasObject.AddComponent<GraphicRaycaster>();

            canvas.renderMode = RenderMode.ScreenSpaceOverlay;
            canvas.pixelPerfect = true;
            /*canvas.worldCamera = Camera.main.camera;
            canvas.planeDistance = 5;*/

            canvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ConstantPixelSize;
        }
    }

}
